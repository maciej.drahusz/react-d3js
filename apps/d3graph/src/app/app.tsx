import styled from 'styled-components';
import { FeaturesBarChartJsx } from '@d3graph/features/bar-chart-jsx';

const StyledApp = styled.div`
  height: calc(100vh - 16px);
  background: black;
  color: white;
`;

export function App() {
  return (
    <StyledApp>
      <FeaturesBarChartJsx />
      {/* <FeaturesBarChart /> */}
    </StyledApp>
  );
}

export default App;
