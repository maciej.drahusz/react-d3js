import styled from 'styled-components';
import { useRef, useEffect, useState } from 'react';
import * as d3 from 'd3';

const StyledFeaturesBarChartJsx = styled.div`
  height: 100%;
`;

const StyledSVG = styled.svg``;
const StyledText = styled.text`
  fill: white;
  font-size: 22px;
  text-anchor: middle;
`;

export const StyledAxis = styled.g`
  color: white;
`;

const dimensions = {
  top: 80,
  left: 60,
  bottom: 60,
  right: 60
};

const createData = (data) =>
  data.map((d, i) => {
    return {
      id: i + 1,
      value: d
    };
  });

const colors = d3.scaleOrdinal(d3.schemeCategory10);

const Bar = ({ data, x, y, height }) => {
  return (
    <g transform={`translate(${x(data.id)}, ${y(data.value)})`}>
      <rect
        width={x.bandwidth()}
        height={height - dimensions.bottom - dimensions.top - y(data.value)}
        fill={colors(data.id)}
      />
      <StyledText transform={`translate(${x.bandwidth() / 2 - 5}, ${-4})`}>
        {data.value}
      </StyledText>
    </g>
  );
};

const XAxis = ({ scale, height }) => {
  const axisRef = useRef(null);

  useEffect(() => {
    d3.select(axisRef.current).call(d3.axisBottom(scale));
  });

  return (
    <StyledAxis
      ref={axisRef}
      transform={`translate(${dimensions.left}, ${height - dimensions.bottom})`}
    />
  );
};

const YAxis = ({ scale }) => {
  const axisRef = useRef(null);

  useEffect(() => {
    d3.select(axisRef.current).call(d3.axisLeft(scale));
  });

  return (
    <StyledAxis
      ref={axisRef}
      transform={`translate(${dimensions.left}, ${dimensions.top})`}
    />
  );
};

export function FeaturesBarChartJsx(props) {
  const wrapperRef = useRef(null);
  const [size, setSize] = useState({ width: 0, height: 0 });

  const [data, setData] = useState(
    createData([10, 40, 30, 20, 50, 10, 33, 78])
  );

  useEffect(() => {
    const wrapper = d3
      .select(wrapperRef.current)
      .node()
      .getBoundingClientRect();
    setSize({ width: wrapper.width, height: wrapper.height });
  }, []);

  if (!size) return null;

  const xAxis = d3
    .scaleBand()
    .range([0, size.width - dimensions.left - dimensions.right])
    .domain(data.map((d) => d.id))
    .paddingInner(0.1)
    .paddingOuter(0.05);

  const yAxis = d3
    .scaleLinear()
    .range([size.height - dimensions.top - dimensions.bottom, 0])
    .domain([0, d3.max(data.map((d) => d.value))]);

  return (
    <StyledFeaturesBarChartJsx ref={wrapperRef}>
      <StyledSVG width={size.width} height={size.height}>
        <StyledText
          transform={`translate(${size.width / 2 - 50}, ${dimensions.top})`}
        >
          Bar chart with some numers
        </StyledText>
        <XAxis scale={xAxis} height={size.height} />
        <YAxis scale={yAxis} />
        <g transform={`translate(${dimensions.left}, ${dimensions.top})`}>
          {data.map((d) => {
            return <Bar data={d} x={xAxis} y={yAxis} height={size.height} />;
          })}
        </g>
      </StyledSVG>
    </StyledFeaturesBarChartJsx>
  );
}
export default FeaturesBarChartJsx;
