import { render } from '@testing-library/react';
import FeaturesBarChartJsx from './features-bar-chart-jsx';
describe('FeaturesBarChartJsx', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<FeaturesBarChartJsx />);
    expect(baseElement).toBeTruthy();
  });
});
