import styled from 'styled-components';
import { useRef, useState, useEffect } from 'react';
import * as d3 from 'd3';

const StyledFeaturesBarChart = styled.div`
  height: 100%;
`;

const dimensions = {
  top: 80,
  left: 60,
  bottom: 60,
  right: 60
};

const colors = d3.scaleOrdinal(d3.schemeCategory10);

const BarChart = ({ data, width, height }) => {
  const ref = useRef(null);

  useEffect(() => {
    const svg = d3.select(ref.current);
    svg
      .append('text')
      .text('Bar chart with some numbers')
      .attr('fill', 'white')
      .attr('transform', `translate(${width / 2 - 50}, ${30} )`)
      .style('font-size', 24);

    const selection = svg.selectAll('rect').data(data);
    const enter = selection.enter();

    const xScale = d3
      .scaleBand()
      .range([0, width - dimensions.left - dimensions.right])
      .domain(data)
      .padding(0.1);

    const yScale = d3
      .scaleLinear()
      .domain([d3.max(data), 0])
      .range([0, height - dimensions.top - dimensions.bottom]);

    const bar = enter
      .append('g')
      .attr(
        'transform',
        (d) =>
          `translate(${xScale(d) + dimensions.left}, ${
            yScale(d) + dimensions.top
          } )`
      );

    bar
      .append('rect')
      .attr('width', xScale.bandwidth())
      .attr(
        'height',
        (d) => height - yScale(d) - dimensions.bottom - dimensions.top
      )
      .attr('fill', (d) => colors(d));

    bar
      .append('text')
      .text((d) => d)
      .attr('fill', 'white')
      .attr('transform', `translate(${xScale.bandwidth() / 2 - 10}, ${-10} )`);

    svg
      .append('g')
      .call(d3.axisLeft(yScale))
      .style('color', 'white')
      .attr('transform', `translate(${dimensions.left}, ${dimensions.top} )`);

    svg
      .append('g')
      .call(d3.axisBottom(xScale))
      .attr(
        'transform',
        `translate(${dimensions.left}, ${height - dimensions.bottom} )`
      )
      .style('color', 'white');
  }, [data, height, width]);

  return <svg ref={ref} width={width} height={height}></svg>;
};

export function FeaturesBarChart() {
  const ref = useRef(null);
  const [size, setSize] = useState(null);
  const [data, setData] = useState([10, 40, 30, 20, 50, 10, 33, 78]);

  useEffect(() => {
    const wrapper = d3.select(ref.current).node().getBoundingClientRect();
    setSize({ width: wrapper.width, height: wrapper.height });
  }, []);

  return (
    <StyledFeaturesBarChart ref={ref}>
      {size && <BarChart data={data} width={size.width} height={size.height} />}
    </StyledFeaturesBarChart>
  );
}
export default FeaturesBarChart;
